/*S23 Activity*/

//2. Creating database named Hotel
//3. insert a single room
/*Answer:
db.rooms.insertOne(
{
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A Simple Room with all the basic necessities"
});*/

//4. Update or insert rooms available and isAvailable:
/*Answer:
db.rooms.updateOne(
{
    "_id": ObjectId("6135ffd884298aabc52bc94a")
},
{
    $set: {
        "rooms_available": 10,
        "isAvailable": false
    }
});*/

//5. Insert Multiple rooms
/*Answer:
db.rooms.insertMany([
{"name": "queen", "accomodates": 4, "price": 4000, "description": "A room with a queen sized bed perfect for a simple getaway", "rooms_available": 15, "isAvailable": false},
{"name": "double", "accomodates": 3, "price": 2000, "description": "A room fit for a small family going on a vacation", "rooms_available": 5, "isAvailable": false}
]);*/

//6. Use the find method and search for a room with the name double
/*Answer:
db.rooms.find({ "name": "double" });*/

//7. Update queen rooms available
/*Answer:
db.rooms.updateOne(
{
    "rooms_available": 15
},
{
    $set: {
        "rooms_available": 0,
    }
});*/

//8. delete the rooms with 0 availability
/*Answer:
db.rooms.deleteMany({ "rooms_available": 0 });*/